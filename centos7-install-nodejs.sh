#!/bin/bash
if which node > /dev/null; then
	echo node exist
else
	if which wget > /dev/null; then
    		echo wget exist continue installing
	else
		sudo yum install wget
	fi
	
	curl -sL https://rpm.nodesource.com/setup_10.x | bash -
	sudo yum install -y nodejs
	sudo yum install gcc gcc-c++ make
	node --version
	npm --version
fi


