#!/bin/bash
if which docker > /dev/null; then

sudo yum remove docker \
                docker-client \
                docker-client-latest \
                docker-common \
                docker-latest \
                docker-latest-logrotate \
                docker-logrotate \
                docker-engine

else

echo 'Docker is not exist'
fi