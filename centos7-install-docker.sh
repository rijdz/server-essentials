
if which docker > /dev/null; then
echo 'Docker already exist'
docker -v

else

echo '>>>>>>> Pre-requisite'
sudo yum install -y yum-utils \
device-mapper-persistent-data \
lvm2
echo '>>>>>>> Install Stable version of Docker'
sudo yum-config-manager \
--add-repo \
https://download.docker.com/linux/centos/docker-ce.repo
echo '>>>>>>> Install docker engine'
sudo yum install -y docker-ce docker-ce-cli containerd.io
echo '>>>>>>> Starting up docker engine'
sudo systemctl start docker
echo '>>>>>>> Verify Hello World'
sudo docker run hello-world
echo '>>>>>>> Fin'
docker -v
fi
