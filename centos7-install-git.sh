#!/bin/bash
if which git > /dev/null; then
echo 'GIT already exist'
git --version
else
echo '>>>>>>> Pre-requisite'
yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel
yum install gcc perl-ExtUtils-MakeMaker
echo '>>>>>>> Install Git'
cd /usr/src
wget https://www.kernel.org/pub/software/scm/git/git-2.21.0.tar.gz
tar xzf git-2.21.0.tar.gz
echo '>>>>>>> Compile'
cd git-2.21.0
make prefix=/usr/local/git all
make prefix=/usr/local/git install
echo '>>>>>>> Setup environment'
echo "export PATH=/usr/local/git/bin:$PATH" >> /etc/bashrc
source /etc/bashrc
echo '>>>>>>> Check Version'
git --version
fi
