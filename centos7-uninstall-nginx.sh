#!/bin/bash
echo 'Stop the service'
sudo systemctl stop nginx
sudo systemctl disable nginx
echo 'Remove folder nginx related'
sudo userdel -r nginx
chkconfig nginx off
sudo rm -rf /etc/nginx
sudo rm -rf /var/log/nginx
sudo rm -rf /var/cache/nginx/
sudo rm -rf /usr/lib/systemd/system/nginx.service
yum remove nginx
