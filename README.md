# Server Essentials

Rijdz collection of script mostly on Linux environment

## Installation

On server setup repository folder

```bash
mkdir /var/repository && cd /var/repository
git clone https://gitlab.com/rijdz/server-essentials.git
```

## Usage

Don't forget to set script as executable
```bash
chmod +x /path/file.sh
```

To accept all command
```bash
yes | bash /var/repository/install-something.sh
```


## License
[MIT](https://choosealicense.com/licenses/mit/)
