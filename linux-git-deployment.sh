#!/bin/bash


if which git > /dev/null; then
	/usr/bin/printf "\xE2\x9C\x94 Git Installed \n"
else
	echo 'Please install git first'
	exit 9999
fi

default_path="/var/repository"
default_www_path="/var/www"

if [ -d $default_path ] ; then
	/usr/bin/printf "\xE2\x9C\x94 Default path exist\n"
else
	mkdir /var/repository
fi

if [ -d $default_www_path ] ; then
	/usr/bin/printf "\xE2\x9C\x94 Default www path exist\n"
else
	mkdir /var/www
fi

read -p $'Please enter the name of repository\n(e.g project-very-cool) => ' repository_name

[[ -z "$repository_name" ]] && /usr/bin/printf "\xE2\x9C\x95 repository name are not allowed to be empty \n"  && exit 9999

repository_path=${default_path}/${repository_name}

git_path=${repository_path}.git
if [ -d $git_path ]; then
	/usr/bin/printf "\xE2\x9C\x94 Repository path exist\n"
	echo "Delete ${git_path} first then run one more time"
	exit 9999
else
	echo '>>>>>>> Initialize Git'
	cd /var/www && mkdir $repository_name
	echo "/var/www/${git_name} created"
	git_name=${repository_name}.git
	cd /var/repository && mkdir $git_name
	echo "${git_name} created"
	cd $git_name && pwd
	git init --bare
	
	echo '>>>>>>> Create hooks'
	repository_hooks="git --work-tree=/var/www/${repository_name} --git-dir=${git_path} checkout -f"
	cd hooks
	echo -e "#!/bin/sh\n$repository_hooks" >> post-receive
	chmod +x post-receive
fi

/usr/bin/printf "\xE2\x9C\x94 Remote repository $git_path is ready\n"
echo '>>>fin<<<'

