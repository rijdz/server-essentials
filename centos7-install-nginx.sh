#!/bin/bash
if which nginx > /dev/null; then
	echo nginx already exist
else
	sudo yum install epel-release
	yum install nginx -y
	
	echo '>>>>>>> Configure the server for NGINX'
	echo 'Enabling Nginx'
	sudo systemctl enable nginx
	echo 'Starting Nginx'
	sudo systemctl start nginx
	echo 'Status Nginx'
	sudo systemctl status nginx
	echo '>>>>>>> Install firewalld'
	sudo yum install firewalld
	sudo systemctl start firewalld
	sudo systemctl enable firewalld
	sudo systemctl status firewalld	
	echo '>>>>>>> Setup the firewall-cmd'
	sudo firewall-cmd --permanent --zone=public --add-service=http
	sudo firewall-cmd --permanent --zone=public --add-service=https
	sudo firewall-cmd --reload
	nginx -v
	
fi
